## Pretz.css

*A Thin, Unadorned CSS Pattern Library for Base Styles*

![pretz](http://www.seriouseats.com/images/20110512-japanese-snacks-pretz-lined-up.jpg)

- Normalize Browser Styles with [normalize.css](https://necolas.github.io/normalize.css/)
- Mimic the [Most Boringest Parts of Bootstrap](https://webdesign.tutsplus.com/courses/building-your-own-pattern-library-for-the-web)
- Built with [Sass](http://sass-lang.com/) using [node-sass](https://github.com/sass/node-sass)
